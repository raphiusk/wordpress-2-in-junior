<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '456852' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'A/ewm!+>*HG+zN4[u/)G4D3+m^+#!&qlhdJ}7EtHOtYd`ep@xh#!ZuNgv-ey,=<H' );
define( 'SECURE_AUTH_KEY',  'qj$PJ PexrmRh^v(N4y<5|rbL<=#@ox(pJtvK)WFpWuw$tJPGJuS}p72rHBQq$7$' );
define( 'LOGGED_IN_KEY',    'FnQ2-WG[sKxon3<hnI10OO|[?;n,wJ^1odWDxK jKW?n?$XSM.KD@m@q|0A+(yuu' );
define( 'NONCE_KEY',        '@wWkc<[m<qUHd-[e&`F|8?S|,Fd&fgIk7oQpcjTCwY`%)cFe1hn#;C[gp4^b,5Ae' );
define( 'AUTH_SALT',        'p::C5+;z&N!X!;)T~@aC[N,?<Mz<a[Y(SA2yF]E_v;9ST12*)8J!ScRlxoD7_R/x' );
define( 'SECURE_AUTH_SALT', '2@:g7vVSdniZ2#ZETYfB:eLcu6cHO>wqch;3$$e_!sfy@_4wpA1z%{qhp*wg+FVh' );
define( 'LOGGED_IN_SALT',   'qli!ABRRub8p!Q>><;+&r2WP^WYB##7Mfz}vxO5&yn2 9fR/<l0e j~ZwksrT=(l' );
define( 'NONCE_SALT',       '<K#H.9K_.3.kCFUZ !HHmX%JymqAhd|j&RKXRQw >ZLjttu:y S:1A=cX5u)zH$1' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
